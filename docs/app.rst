***
App
***

App goals
=========

Voice Assistant App for Android powered smartphones.

Informing user about objects and obstacles in front of him by simple voice commands.

Detecting dangerous objects by analyzing image from camera and measuring distance to them using data from the proximity sensor.

Useful personal assistant specified to be blind people friendly.

