****
Band
****

Band goals
==========

band goals

Band hardware specification
===========================

Our Band will be build from cheap and easy to setup components.

.. image:: https://i.imgur.com/HN5mbqJ.png
    :alt: schematic

Components:
-----------

* ArduCam OV2640 2MPx 1600x1200px 60fps

* NRF518221

* HC-SR04 2-400cm2

* Arduino Nano (ATmega328)

ArduCam OV2640 2MPx 1600x1200px 60fps
-------------------------------------

ArduCam OV2640 is cheap and popular camera. It's simplicity allows us to interface it with arduino and bluetooth module.

NRF518221
---------

NRF518221 comes with it's own control circuit. Thanks to that we can connect it directly to ArduCam. This module comes with full support for bluetooth and other 2,4GHZ type wireless comunication methods, while consumpting very low energy. 

HC-SR04 2-400cm2
----------------

HC-SR04 is popular distance sensor. Data from it will be interpreted by arduino board and send with picture data by bluetooth.

Arduino Nano (ATmega328)
------------------------

Arduino is very popular multipurpose controlling board. In our device we will be using it to controll bluetooth module, camera and distance sensor.

Band software specification
===========================

Band software is going to be focused on communication with phone over bluetooth. It will combine 2 separate programs. One running on Arduino board and second
on NRF518221. They will communicate with eachother using 2 pins in digital mode. Data from ArduCam will be transfered directly to NRF518221. Arduino will only
add data from distance sensor and synchronise 2 modules. Sending and managing packets will be done directly on NRF518221.

More technical documentation is going to be written in some time.