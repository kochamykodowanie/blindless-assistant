*****
Cloud
*****

Cloud goals
===========

Blindless cloud will be system for communicating between Apps, Apis and for managing data in system. Mainly it will store data for APIs.

Technical assumptions
=====================

Data will be stored in Mongo Db databases.

All microservices will be working in Docker containers.

Primary language will be Golang.