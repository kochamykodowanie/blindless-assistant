*********
Blindless
*********

Blindless is assistant for blind people, that helps them when moving around.

.. toctree::
    :maxdepth: 2

    :caption: Project parts

    band.rst
    app.rst
    api.rst
    cloud.rst
